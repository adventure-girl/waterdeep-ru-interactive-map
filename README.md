## 🏰📍 Карта Вотердипа

Также доступна на сайте [Adventure Girl](https://adventure-girl.vercel.app).

### Обратная связь
Пишите об ошибках, предлагайте исправления и улучшения через специальный [Discord-сервер](https://discord.gg/SPpmTmzXXC).
Всех активных участников добавлю на сайт как соавторов карты!

### Поддержать автора
Поддержать автора можно подпиской на:
- [Boosty](https://boosty.to/omar0275)
- [Youtube](https://youtube.com/@AdventureGirl0275)
- [VK](https://vk.com/omar0275)
